CREATE TABLE [dbo].[ulocateGeocodedAddress](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NULL,
	[address1] [nvarchar](255) NULL,
	[address2] [nvarchar](255) NULL,
	[locality] [nvarchar](50) NULL,
	[region] [nvarchar](50) NULL,
	[postalCode] [nvarchar](50) NULL,
	[countryCode] [nvarchar](2) NULL,
	[comment] [nvarchar](500) NULL,
	[coordinateGeography] [geography] NULL,
	[viewportGeography] [geography] NULL,
	[geoCodeStatus] [nvarchar](20) NULL,
	[extendedDataXml] [nvarchar](max) NULL,
	[createDate] [datetime] NOT NULL,
	[updateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_uLocateAddress] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

ALTER TABLE [dbo].[ulocateGeocodedAddress] ADD  CONSTRAINT [DF_ulocateAddress_address2]  DEFAULT (NULL) FOR [address2]

ALTER TABLE [dbo].[ulocateGeocodedAddress] ADD  CONSTRAINT [DF_ulocateAddress_coordinateGeography]  DEFAULT (NULL) FOR [coordinateGeography]

ALTER TABLE [dbo].[ulocateGeocodedAddress] ADD  CONSTRAINT [DF_ulocateAddress_viewportGeography]  DEFAULT (NULL) FOR [viewportGeography]

ALTER TABLE [dbo].[ulocateGeocodedAddress] ADD  CONSTRAINT [DF_ulocateAddress_createDate]  DEFAULT (getdate()) FOR [createDate]

ALTER TABLE [dbo].[ulocateGeocodedAddress] ADD  CONSTRAINT [DF_ulocateAddress_updateDate]  DEFAULT (getdate()) FOR [updateDate]

