﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using __ = umbraco.cms.businesslogic.datatype;
using umbraco.editorControls;

namespace uLocate.DataTypes.GeocodedAddress
{
    /// <summary>
    /// The PreValue Editor for the GeocodedAddress data-type.
    /// </summary>
    public class GeocodedAddressPrevalueEditor : AbstractJsonPrevalueEditor
    {
        private CheckBox ShowNameField;

        /// <summary>
        /// Initializes a new instance of the <see cref="GeocodedAddressPrevalueEditor"/> class.
        /// </summary>
        /// <param name="dataType">Type of the data.</param>
        public GeocodedAddressPrevalueEditor(__.BaseDataType dataType)
            : base(dataType, __.DBTypes.Nvarchar)
        {
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            // set the options
            var options = new GeocodedAddressOptions(true)
            {
                ShowNameField = this.ShowNameField.Checked
            };

            // save the options as JSON
            this.SaveAsJson(options);
        }

        /// <summary>
        /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
        /// </summary>
        protected override void CreateChildControls()
        {
            base.CreateChildControls();

            // set-up child controls
            this.ShowNameField = new CheckBox() { ID = "ShowNameField" };

            // add the child controls
            this.Controls.AddPrevalueControls(this.ShowNameField);
        }

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"/> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // get PreValues, load them into the controls.
            var options = this.GetPreValueOptions<GeocodedAddressOptions>();

            // no options? use the default ones.
            if (options == null)
            {
                options = new GeocodedAddressOptions(true);
            }

            // set the values
            this.ShowNameField.Checked = options.ShowNameField;
        }

        /// <summary>
        /// Renders the contents of the control to the specified writer. This method is used primarily by control developers.
        /// </summary>
        /// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
        protected override void RenderContents(HtmlTextWriter writer)
        {
            // add property fields
            writer.AddPrevalueRow("Show name field?", this.ShowNameField);
        }
    }
}