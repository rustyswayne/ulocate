﻿using System;
using System.Collections.Generic;
using System.Linq;
using uLocate.Core.Persistence.Caching;
using Umbraco.Core;
using Umbraco.Core.Models.EntityBase;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.UnitOfWork;
using Entity = uLocate.Core.Models.EntityBase.Entity;


namespace uLocate.Core.Persistence.Repositories
{
    /// <summary>
    /// Represent an abstract Repository, which is the base of the Repository implementations
    /// </summary>
    /// <typeparam name="TEntity">Type of entity for which the repository is used</typeparam>
    /// <typeparam name="TId">Type of the Id used for this entity</typeparam>
    /// <remarks>Extremeley Simplified Variation of Umbraco.Core.Persistence.Repositories.RepositoryBase</remarks>
    internal abstract class RepositoryAbstract<TId, TEntity> : DisposableObject, IUnitOfWorkRepository  //, IRepository<TId, TEntity>
        where TEntity : Entity
    {
                
        private readonly IUnitOfWork _work;


        protected RepositoryAbstract(IUnitOfWork work)
        {
            _work = work;            
        }

        /// <summary>
        /// Returns the Unit of Work added to the repository
        /// </summary>
        protected internal IUnitOfWork UnitOfWork
        {
            get { return _work; }
        }

        #region Abstract Methods

        protected abstract Sql GetBaseQuery(bool isCount);
        protected abstract string GetBaseWhereClause();
        protected abstract IEnumerable<string> GetDeleteClauses();
        protected abstract TEntity PerformGet(TId id);
        protected abstract IEnumerable<TEntity> PerformGetAll(params TId[] ids);        
        protected abstract bool PerformExists(TId id);
        protected abstract int PerformCount(Sql sql);

        public abstract void Delete(TEntity entity);

#endregion


        public virtual void PersistNewItem(IEntity entity)
        {
            PersistNewItem((TEntity) entity);
        }

        public virtual void PersistUpdatedItem(IEntity entity)
        {
            PersistUpdatedItem((TEntity) entity);
        }
        public virtual void PersistDeletedItem(IEntity entity)
        {
            PersisteDeletedItem((TEntity)entity);
        }

        #region IUnitOfWorkRepository Methods

        protected abstract void PersistNewItem(TEntity entity);
        protected abstract void PersistUpdatedItem(TEntity entity);
        protected abstract void PersisteDeletedItem(TEntity entity);

        #endregion

        #region IRepository<TEntity> Members

        /// <summary>
        /// Adds or Updates an entity of type TEntity
        /// </summary>
        /// <remarks>This method is backed by an <see cref="IRepositoryCacheProvider"/> cache</remarks>
        /// <param name="entity"></param>
        public void AddOrUpdate(TEntity entity)
        {
            if (entity.Id > 0)
            {
                entity.UpdatingEntity();
                PersistUpdatedItem(entity);                
            }
            else
            {
                entity.AddingEntity();
                PersistNewItem(entity);
            }
        }


        
        /// <summary>
        /// Gets an entity by the passed in Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TEntity Get(TId id)
        {            
            var entity = PerformGet(id);

            return entity;
        }

        
        /// <summary>
        /// Gets all entities of type TEntity or a list according to the passed in Ids
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public IEnumerable<TEntity> GetAll(params TId[] ids)
        {
           
            var entityCollection = PerformGetAll(ids);

            return entityCollection;
        }


        /// <summary>
        /// Returns a boolean indicating whether an entity with the passed Id exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Exists(TId id)
        {     
            return PerformExists(id);
        }

        #endregion

        protected override void DisposeResources()
        {
            UnitOfWork.DisposeIfDisposable();
        }


    }
}