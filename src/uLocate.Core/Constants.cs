﻿using System.Configuration;
using uLocate.Core.Configuration;
using uLocate.Core.Models;

namespace uLocate.Core
{
    public partial class Constants
    {
        /// <summary>
        /// Spacial Reference ID - World Geodetic System
        /// </summary>
        public const int SRID_WorldGeodeticSystem = 4326;

        /// <summary>
        /// Name of the application.
        /// </summary>
        public const string ApplicationName = "uLocate";

        public static uLocateSection Configuration
        {
            get { return (uLocateSection)ConfigurationManager.GetSection(ApplicationName); }
        }

        public struct Urls
        {
            public const string GeocodedAddressEditPage = "plugins/uLocate/EditGeocodedAddress.aspx";
        }

        public struct MediaTypeNames
        {
            public struct Application
            {
                /// <summary>
                /// MIME type for JavaScript files/scripts.
                /// </summary>
                public const string JavaScript = "application/x-javascript";

                /// <summary>
                /// MIME type for JSON text.
                /// </summary>
                public const string Json = "application/json";
            }

            /// <summary>
            /// Class containing MIME type constants for text files.
            /// </summary>
            public struct Text
            {
                /// <summary>
                /// MIME type for Cascading StyleSheet files.
                /// </summary>
                public const string Css = "text/css";

                /// <summary>
                /// MIME type for Comma-Seperated Values files.
                /// </summary>
                public const string Csv = "text/csv";
            }
        }

    }
}