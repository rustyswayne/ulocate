﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace uLocate.Core.Models
{
    [Serializable]
    [DataContract(IsReference = true)]
    public class GeocodeResponse : IGeocodeResponse
    {
       
        /// <summary>
        /// GeocodeResponse constructor
        /// </summary>
        public GeocodeResponse(GeocodeStatus status, IEnumerable<IGeocode> results)
        {
            _status = status;
            _results = results;
        }

        private GeocodeStatus _status;
        /// <summary>
        /// The interpreted status value
        /// </summary>
        [DataMember(Name="quality")]
        public GeocodeStatus Status { get { return _status; } }

        private IEnumerable<IGeocode> _results;
        /// <summary>
        /// The collection of resulting geocodes matching the query
        /// </summary>
        [DataMember(Name="results")]
        public IEnumerable<IGeocode> Results { get { return _results; } }

    }
}
