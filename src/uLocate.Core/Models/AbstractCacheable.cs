﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Web;

namespace uLocate.Core.Models
{
    public abstract class AbstractCacheable
    {

        /// <summary>
        /// Quick reference to the Cache object
        /// </summary>
        protected static System.Web.Caching.Cache Cache
        {
            get { return HttpContext.Current.Cache; }
        }

        /// <summary>
        /// Purges the current http cache of items with keys by prefix
        /// </summary>
        /// <remarks>
        /// This is sort of like a wildcard purge of cache
        /// </remarks>
        protected static void PurgeCacheItems(string prefix)
        {
            // assert the prefix is lower case
            prefix = prefix.ToLowerInvariant();

            // create a list of matches to remove from cache
            List<string> itemsToRemove = new List<string>();
            IDictionaryEnumerator enumerator = Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                // if the enumerating key matches the prefix - add it to the list to be purged
                if (enumerator.Key.ToString().ToLowerInvariant().StartsWith(prefix)) itemsToRemove.Add(enumerator.Key.ToString());
            }

            // remove the items from cache
            foreach (var item in itemsToRemove)
            {
                Cache.Remove(item);
            }

        }
    }
}
