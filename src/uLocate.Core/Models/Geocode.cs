﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace uLocate.Core.Models
{
    [Serializable]
    [DataContract(IsReference = true)]
    public class Geocode : IGeocode
    {
        private string _formattedAddress;
        private ICoordinate _coordinate;
        private GeocodeQuality _quality;
        private Viewport _viewport;

        /// <summary>
        /// Constructor for Geocode
        /// </summary>
        public Geocode(string formattedAddress, double latitude, double longitude, GeocodeQuality quality, Viewport viewport = null)
        {
            _formattedAddress = formattedAddress;
            _coordinate = new Coordinate(latitude, longitude);
            _quality = quality;
            _viewport = viewport;
        }

        /// <summary>
        /// The formatted address returned from the API
        /// </summary>
        [DataMember(Name="formatted_address")]
        public string FormattedAddress { get { return _formattedAddress; } }

        /// <summary>
        /// The Latitude value
        /// </summary>
        [DataMember(Name="latitude")]
        public double Latitude { get { return _coordinate.Latitude; } }

        /// <summary>
        /// The Longitude value
        /// </summary>
        [DataMember(Name="longitude")]
        public double Longitude { get { return _coordinate.Longitude; } }

        /// <summary>
        /// The API returned quality of the geocode
        /// Interpretted from Google's "location_type" or Yahoo! "Quality" of result
        /// </summary>
        [DataMember(Name="quality")]
        public GeocodeQuality Quality { get { return _quality; } }

        /// <summary>
        /// Recommended Viewport for displaying the Geographic to the user
        /// </summary>
        [DataMember(Name="viewport")]
        public Viewport Viewport { get { return _viewport; } }

        /// <summary>
        /// Helper method to format an address as a string that can be used in a GeocodeService provider call
        /// </summary>
        public static string FormatAddress(GeocodedAddress geoAddress)
        {
            return FormatAddress(geoAddress.Address1, geoAddress.Address2, geoAddress.Locality, geoAddress.Region, geoAddress.PostalCode, geoAddress.CountryCode);
        }

        /// <summary>
        /// Helper method to format an address as a string that can be used in a GeocodeService provider call
        /// </summary>
        public static string FormatAddress(string address1, string address2, string locality, string region, string postalCode, string countryCode)
        {
            var segments = new[]
            {
                string.Concat(address1, " ", address2),
                locality ?? string.Empty,
                string.Concat(region, " ", postalCode),
                countryCode ?? string.Empty
            };

            return string.Join(", ", segments
                .Select(x => x.Trim())
                .Where(x => !string.IsNullOrWhiteSpace(x)));
        }
    }
}