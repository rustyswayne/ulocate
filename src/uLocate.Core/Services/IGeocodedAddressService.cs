﻿using System;
using System.Collections.Generic;
using Umbraco.Core.Services;
using uLocate.Core.Models;

namespace uLocate.Core.Services
{
    public interface IGeocodedAddressService  : IService
    {

        /// <summary>
        /// Creates a <see cref="GeocodedAddress"/> object
        /// </summary>
        /// <param name="name">Name of the GeocodedAddress object</param>
        /// <returns></returns>
        IGeocodedAddress CreateGeocodedAddress(string name);

        IEnumerable<IGeocodedAddress> GetAll();
        
        /// <summary>
        /// Gets an <see cref="IGeocodedAddress"/> object by Id
        /// </summary>
        /// <param name="id">Id of the GeocodedAddress to retrieve</param>
        /// <returns><see cref="IGeocodedAddress"/></returns>
        IGeocodedAddress GetById(int id);

        /// <summary>
        /// Saves a single <see cref="IGeocodedAddress"/> object
        /// </summary>
        /// <param name="address">The <see cref="IGeocodedAddress"/> to save</param>
        /// <param name="forceGeocodingIfAlreadyPresent"></param>
        void Save(IGeocodedAddress address, bool forceGeocodingIfAlreadyPresent = false);

        /// <summary>
        /// Saves a collection of <see cref="IGeocodedAddress"/> objects
        /// </summary>
        /// <param name="addresses"></param>
        /// <param name="forceGeocodingIfAlreadyPresent"></param>
        void Save(IEnumerable<IGeocodedAddress> addresses, bool forceGeocodingIfAlreadyPresent = false);

        /// <summary>
        /// Deletes a single <see cref="IGeocodedAddress"/> object
        /// </summary>
        /// <param name="geoAddress">The <see cref="IGeocodedAddress"/> to delete</param>
        void Delete(IGeocodedAddress geoAddress);

        /// <summary>
        /// Deletes a collection of <see cref="IGeocodedAddress"/> objects
        /// </summary>
        /// <param name="addresses"></param>
        void Delete(IEnumerable<IGeocodedAddress> addresses);


        /// <summary>
        /// Geocodes a single address
        /// </summary>
        /// <param name="address"><see cref="IGeocodedAddress"/></param>
        /// <param name="overwrite"></param>
        /// <returns></returns>
        IGeocodedAddress LookupGeocode(IGeocodedAddress address, bool overwrite = false);

        /// <summary>
        /// Geocodes a collection of addresses
        /// </summary>
        /// <param name="addresses"><see cref="IGeocodedAddress"/></param>
        /// <param name="overwrite"></param>
        IEnumerable<IGeocodedAddress> LookupGeocode(IEnumerable<IGeocodedAddress> addresses, bool overwrite = false);

    }
}
