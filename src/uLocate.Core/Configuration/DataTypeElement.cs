﻿using System.Configuration;

namespace uLocate.Core.Configuration
{
    public class DataTypeElement : ConfigurationElement
    {
        /// <summary>
        /// Gets/sets the name (key) value for the data type collection element
        /// </summary>
        [ConfigurationProperty("name", IsKey = true)]
        public string Name
        {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }
    }
}
