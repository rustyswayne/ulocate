﻿using System.Configuration;

namespace uLocate.Core.Configuration
{
    public class SettingsCollection : ConfigurationElementCollection
    {
        /// <summary>
        /// Creates a new <see cref="ConfigurationElement">ConfigurationElement</see>.
        /// CreateNewElement must be overridden in classes that derive from the ConfigurationElementCollection class.
        /// </summary>
        protected override ConfigurationElement CreateNewElement()
        {
            return new SettingElement();
        }

        /// <summary>
        /// Gets the element key for a specified configuration element when overridden in a derived class.
        /// </summary>
        /// <param name="element">SettingElement</param>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SettingElement)element).Name;
        }

        /// <summary>
        /// Default. Returns the SettingElement with the index of index from the collection
        /// </summary>
        public SettingElement this[object index]
        {
            get { return (SettingElement)this.BaseGet(index); }
        }
    }
}
