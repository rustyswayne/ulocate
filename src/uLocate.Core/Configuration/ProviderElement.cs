﻿using System.Configuration;

namespace uLocate.Core.Configuration
{
    public class ProviderElement : ConfigurationElement
    {
        /// <summary>
        /// Gets/sets the name (key) value for the provider collection element
        /// </summary>
        [ConfigurationProperty("name", IsKey = true)]
        public string Name
        {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }

        /// <summary>
        /// Gets/sets the providerType attribute
        /// </summary>
        [ConfigurationProperty("providerType", IsRequired=true)]
        public string ProviderType
        {
            get { return (string)this["providerType"]; }
            set { this["providerType"] = value; }
        }

        /// <summary>
        /// Get/sets the enableCaching value
        /// </summary>
        [ConfigurationProperty("enableCaching", IsRequired=true)]
        public bool EnableCaching
        {
            get { return (bool)this["enableCaching"]; }
            set { this["enableCaching"] = value; }
        }


        /// <summary>
        /// Gets/sets the cacheDuration value in seconds.
        /// </summary>
        [ConfigurationProperty("cacheDuration", IsRequired=true)]
        public int CacheDuration
        {
            get { return (int)this["cacheDuration"]; }
            set { this["cacheDuration"] = value; }
        }

        /// <summary>
        /// Gets/sets the geocodeLimit value. This is the limit of requests that can/should be done 
        /// at any given time to the provider.
        /// </summary>
        [ConfigurationProperty("geocodeLimit", IsRequired=false, DefaultValue=500)]
        public int GeocodeLimit
        {
            get { return (int)this["geocodeLimit"]; }
            set { this["geocodeLimit"] = value; }
        }

        /// <summary>
        /// Gets/sets a toggle to indicate whether or not the provider should log its geocode requests
        /// </summary>
        [ConfigurationProperty("logRequests", IsRequired = false, DefaultValue = false)]
        public bool LogRequests
        {
            get { return (bool)this["logRequests"]; }
            set { this["logRequests"] = value; }
        }

        /// <summary>
        /// Gets/sets settings for the provider
        /// </summary>
        [ConfigurationProperty("settings", IsRequired = false), ConfigurationCollection(typeof(SettingsCollection), AddItemName = "setting")]
        public SettingsCollection Settings
        {
            get { return (SettingsCollection)this["settings"]; }
        }

    }
}
