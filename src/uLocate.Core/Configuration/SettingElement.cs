﻿using System.Configuration;

namespace uLocate.Core.Configuration
{
    public class SettingElement: ConfigurationElement
    {
        /// <summary>
        /// Gets/sets the name (key) value for the settings collection element
        /// </summary>
        [ConfigurationProperty("name", IsKey = true)]
        public string Name
        {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }

        /// <summary>
        /// Gets/sets the value for the settings element
        /// </summary>
        [ConfigurationProperty("value", IsRequired=true)]
        public string Value
        {
            get { return (string)this["value"]; }
            set { this["value"] = value; }
        }
    }
}
