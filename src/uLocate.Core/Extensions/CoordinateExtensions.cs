﻿using Microsoft.SqlServer.Types;
using uLocate.Core.Models;

namespace uLocate.Core.Extensions
{
    public static class CoordinateExtensions
    {
        /// <summary>
        /// Converts <see cref="ICoordinate"/> into <see cref="SqlGeography"/>
        /// </summary>
        /// <returns><see cref="SqlGeography"/> based on the World Geodetic System</returns>
        public static SqlGeography ToSqlGeography(this ICoordinate coordinate)
        {
            return SqlGeography.Point(coordinate.Latitude, coordinate.Longitude, Constants.SRID_WorldGeodeticSystem);
        }

        /// <summary>
        /// Returns true if Lat/Long both have zero values
        /// </summary>
        public static bool IsZeroZero(this ICoordinate coordinate)
        {
            return coordinate.Latitude == 0 && coordinate.Longitude == 0;
        }

        #region Distance

        /// <summary>
        /// Extension of STDistance - converts calculated distance to Kilometers
        /// </summary>
        /// <param name="Other">Object to find shortest distance to</param>
        /// <returns>STDistance returns meters</returns>
        public static double DistanceKilometers(this ICoordinate coordinate, SqlGeography other)
        {
            return coordinate.ToSqlGeography().DistanceKilometers(other);
        }

        /// <summary>
        /// Extension of STDistance - converts calculated distance to Kilometers
        /// </summary>
        public static double DistanceKilometers(this ICoordinate coordinate, ICoordinate other)
        {
            return coordinate.ToSqlGeography().DistanceKilometers(other.ToSqlGeography());
        }

        /// <summary>
        /// Extension of STDistance - converts calculated distance to Kilometers
        /// </summary>
        public static double DistanceKilometers(this ICoordinate coordinate, double latitude, double longitude)
        {
            return coordinate.ToSqlGeography().DistanceKilometers(latitude, longitude);
        }

        /// <summary>
        /// Extension of STDistance - converts calculated distance to miles
        /// </summary>
        /// <param name="Other">Object to find shortest distance to</param>
        /// <returns>STDistance returns meters</returns>
        public static double DistanceMiles(this ICoordinate coordinate, SqlGeography other)
        {
            return coordinate.ToSqlGeography().DistanceMiles(other);
        }

        /// <summary>
        /// Extension of STDistance - converts calculated distance to miles
        /// </summary>
        public static double DistanceMiles(this ICoordinate coordinate, ICoordinate other)
        {
            return coordinate.ToSqlGeography().DistanceMiles(other.ToSqlGeography());
        }

        /// <summary>
        /// Extension of STDistance - converts calculated distance to miles
        /// </summary>
        public static double DistanceMiles(this ICoordinate coordinate, double latitude, double longitude)
        {
            return coordinate.ToSqlGeography().DistanceMiles(latitude, longitude);
        }

        #endregion
    }
}