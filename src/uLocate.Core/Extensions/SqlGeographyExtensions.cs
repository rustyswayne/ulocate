﻿using Microsoft.SqlServer.Types;
using uLocate.Core.Models;

namespace uLocate.Core.Extensions
{
    public static class SqlGeographyExtensions
    {
        /// <summary>
        /// Converts a SqlGeography to a Coordinate
        /// </summary>
        /// <param name="geo"><see cref="SqlGeography"/></param>
        /// <returns><see cref="Coordinate"/></returns>
        public static Coordinate ToCoordinate(this SqlGeography geo)
        {            
            return new Coordinate((double)geo.Lat, (double)geo.Long);
        }

        #region Distance

        /// <summary>
        /// Extension of STDistance - converts calculated distance to Kilometers
        /// </summary>
        /// <param name="Other">Object to find shortest distance to</param>
        /// <returns>STDistance returns meters</returns>
        public static double DistanceKilometers(this SqlGeography geo, SqlGeography other)
        {
            return (double)geo.STDistance(other) / 1000;
        }

        /// <summary>
        /// Extension of STDistance - converts calculated distance to Kilometers
        /// </summary>
        public static double DistanceKilometers(this SqlGeography geo, ICoordinate other)
        {
            return DistanceKilometers(geo, SqlGeography.Point(other.Latitude, other.Longitude, Constants.SRID_WorldGeodeticSystem));
        }

        /// <summary>
        /// Extension of STDistance - converts calculated distance to Kilometers
        /// </summary>
        public static double DistanceKilometers(this SqlGeography geo, double latitude, double longitude)
        {
            return DistanceKilometers(geo, SqlGeography.Point(latitude, longitude, Constants.SRID_WorldGeodeticSystem));
        }

        /// <summary>
        /// Extension of STDistance - converts calculated distance to miles
        /// </summary>
        /// <param name="Other">Object to find shortest distance to</param>
        /// <returns>STDistance returns meters</returns>
        public static double DistanceMiles(this SqlGeography geo, SqlGeography other)
        {
            return (double)(geo.STDistance(other) / 1609.344);
        }

        /// <summary>
        /// Extension of STDistance - converts calculated distance to miles
        /// </summary>
        public static double DistanceMiles(this SqlGeography geo, ICoordinate other)
        {
            return DistanceMiles(geo, SqlGeography.Point(other.Latitude, other.Longitude, Constants.SRID_WorldGeodeticSystem));
        }

        /// <summary>
        /// Extension of STDistance - converts calculated distance to miles
        /// </summary>
        public static double DistanceMiles(this SqlGeography geo, double latitude, double longitude)
        {
            return DistanceMiles(geo, SqlGeography.Point(latitude, longitude, Constants.SRID_WorldGeodeticSystem));
        }

        #endregion
    }
}
