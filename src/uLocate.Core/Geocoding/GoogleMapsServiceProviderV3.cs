﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using uLocate.Core.Configuration;
using uLocate.Core.Models;
using Umbraco.Core.Logging;

namespace uLocate.Core.Geocoding
{
    
    public class GoogleMapsServiceProviderV3 : AbstractGeocodeServiceProvider  
    {
        /// <summary>
        /// GoogleMapsServiceProvider constructor
        /// </summary>
        public GoogleMapsServiceProviderV3()
        {
            var config = Constants.Configuration.Providers["GeocodingAPIProvider"];

            // configure the provider
            GoogleMapsServiceProviderV3.EnableCaching = config.EnableCaching;
            GoogleMapsServiceProviderV3.CacheDuration = config.CacheDuration;
            GoogleMapsServiceProviderV3.LogRequests = config.LogRequests;

            Settings = new Dictionary<string, string>();

            foreach (SettingElement setting in config.Settings)
            {
                Settings.Add(setting.Name, setting.Value);
            }

            //if (!Settings.ContainsKey("APIKey")) Settings.Add("APIKey", string.Empty);
        }

        /// <summary>
        /// Queries the Google Geocoding API for the formattedAddress
        /// </summary>
        public override IGeocodeResponse GetGeocodeResponse(string addressString)
        {
            return GetGeocodeResponse(addressString, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
        }

        /// <summary>
        /// Queries the Google Geocoding API for a geocode
        /// </summary>
        /// <param name="address">The street address value</param>
        /// <param name="locality">The locality or city value</param>
        /// <param name="region">The region or state value</param>
        /// <param name="postalCode">The postal code value</param>
        /// <returns></returns>
        public override IGeocodeResponse GetGeocodeResponse(string address1, string address2, string locality, string region, string postalCode, string countryCode)
        {
            // format the query from the parameters passed
            var q = HttpUtility.UrlEncode(Geocode.FormatAddress(address1, address2, locality, region, postalCode, countryCode));

            // get the url from the configuration
            var url = string.Format(Settings["UrlString"], q);

            //var apiKey = Settings["APIKey"];

            //if (!string.IsNullOrEmpty(apiKey)) url += string.Format("&key={0}", apiKey);

            // create a cacheKey
            var cacheKey = string.Concat("uLocate:GeocodeResponse||", url);

            if (EnableCaching && Cache[cacheKey] != null)
            {
                // this has recently been queried.  used the cached value
                return (GeocodeResponse)Cache[cacheKey];
            }
            else
            {
                if (LogRequests) LogHelper.Info(typeof(GoogleMapsServiceProviderV3), "uLocate is requesting geocode for : " + url);

                // the actual web request to the geocode api
                var req = (HttpWebRequest)WebRequest.Create(url);
                using (var resp = (HttpWebResponse)req.GetResponse())
                {
                    using (var sr = new StreamReader(resp.GetResponseStream()))
                    {
                        // query the api for the response and cache the data
                        var gr = CreateResponse(sr.ReadToEnd());
                        CacheData(cacheKey, gr);
                        return gr;
                    }
                }
            }
        }

        #region Helpers

        /// <summary>
        /// Parses the xml returned from Google Geocoding API
        /// </summary>
        /// <param name="xml">API results xml</param>
        /// <returns>GeocodeResponse with values from the API results returned</returns>
        private GeocodeResponse CreateResponse(string xml)
        {
            List<Geocode> geocodes = new List<Geocode>();

            // Load the xml response
            XDocument response = XDocument.Parse(xml);

            GeocodeStatus _status = (GeocodeStatus)Enum.Parse(typeof(GeocodeStatus), response.Descendants("status").First().Value.ToString());
            
            
            // create the list of resulting geocodes
            if (_status == GeocodeStatus.OK)
            {
                var results = response.Descendants().Where(x => x.Name == "result");

                // TODO : [RS] Consider using the address_components to correct addresses
                foreach (var result in results)
                {
                    var geometry = result.Element("geometry");
                  
                    GeocodeQuality quality = (GeocodeQuality)Enum.Parse(typeof(GeocodeQuality), geometry.Element("location_type").Value.ToString());

                    var location = geometry.Element("location");

                    double latitude = double.Parse(location.Element("lat").Value.ToString());
                    double longitude = double.Parse(location.Element("lng").Value.ToString());

                    var southwest = geometry.Descendants().Where(x => x.Name == "southwest").First();
                    var northeast = geometry.Descendants().Where(x => x.Name == "northeast").First();

                    Viewport vp = new Viewport(
                            new Coordinate(
                                double.Parse(southwest.Element("lat").Value.ToString()),
                                double.Parse(southwest.Element("lng").Value.ToString())
                                ),
                            new Coordinate(
                                double.Parse(northeast.Element("lat").Value.ToString()),
                                double.Parse(northeast.Element("lng").Value.ToString())
                                )
                        );

                    Geocode g = new Geocode(result.Element("formatted_address").Value.ToString(), latitude, longitude, quality, vp);

                   

                    // add the geocode to the result
                    geocodes.Add(g);
                    
                }
                
            }                              

            // prepare the response
            GeocodeResponse resp = new GeocodeResponse(_status, geocodes);

            // return the response
            return resp;
        }

        #endregion
    }
}