﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using umbraco.businesslogic;
using umbraco.interfaces;

namespace uLocate.Web.BackOffice
{
    [Application("ulocate", "uLocate", "uLocate-Icon.gif", sortOrder: 10)]
    public class uLocateApplicationDefinition : IApplication
    {
    }
}