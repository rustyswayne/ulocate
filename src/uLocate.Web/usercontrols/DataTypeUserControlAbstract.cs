﻿using System;
using System.Web;

using umbraco.editorControls;
using umbraco.cms.businesslogic.datatype;
using umbraco.editorControls.userControlGrapper;

using uLocate.Core;

namespace uLocate.Web.UserControls
{
    public abstract class DataTypeUserControlAbstract : System.Web.UI.UserControl, IUsercontrolDataEditor
    {

        protected string umbracoValue;

        #region IUsercontrolDataEditor Members

        public object value
        {
            get { return umbracoValue; }
            set { umbracoValue = value.ToString(); }
        }

        #endregion


   
    }
}