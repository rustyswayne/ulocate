﻿using System;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Web.UI;
using ICSharpCode.SharpZipLib.Zip;
using ServiceStack.Text;
using uLocate.Core;
using Umbraco.Core.IO;

namespace uLocate.Web.UserControls
{
    public partial class ExportAddressData : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            // get all addresses
            var addresses = ULocateHelper.GeocodedAddressList().OrderBy(n => n.Name).ToList();

            var dump = string.Empty;
            var mimeType = "text/plain";
            var fileExtension = ".txt";

            // dump as selected format
            switch (this.rblFormat.SelectedValue)
            {
                case "CSV":
                    dump = CsvSerializer.SerializeToCsv(addresses);
                    mimeType = Constants.MediaTypeNames.Text.Csv;
                    fileExtension = ".csv";
                    break;

                case "JSON":
                    dump = JsonSerializer.SerializeToString(addresses);
                    mimeType = Constants.MediaTypeNames.Application.Json;
                    fileExtension = ".json";
                    break;

                case "XML":
                    dump = XmlSerializer.SerializeToString(addresses);
                    mimeType = MediaTypeNames.Text.Xml;
                    fileExtension = ".xml";
                    break;

                default:
                    break;
            }

            // check if the directory exists
            var exportDir = IOHelper.MapPath("~/App_Data/TEMP/uLocate/");
            if (!Directory.Exists(exportDir))
                Directory.CreateDirectory(exportDir);

            // write dump to export file
            var exportFileName = string.Concat(Constants.ApplicationName, DateTime.UtcNow.ToString("_yyyyMMddHHmmss"), fileExtension);
            var exportPath = string.Concat(exportDir, exportFileName);
            File.WriteAllText(exportPath, dump);

            // get the bytes from the export script
            var bytes = File.ReadAllBytes(exportPath);

            // check if the export script should be zipped
            if (this.cbZip.Checked)
            {
                using (var zip = new ZipOutputStream(Response.OutputStream))
                {
                    var entry = new ZipEntry(ZipEntry.CleanName(exportFileName));
                    zip.PutNextEntry(entry);
                    zip.Write(bytes, 0, bytes.Length);
                }

                // push the zip file to the browser
                Response.AddHeader("content-disposition", string.Concat("attachment; filename=", exportFileName, ".zip"));
                Response.ContentType = "application/zip";
            }
            else
            {
                // push the export script to the browser
                Response.AddHeader("content-disposition", string.Concat("attachment; filename=", exportFileName));
                Response.ContentType = mimeType;
                Response.BinaryWrite(bytes);
            }

            Response.Flush();
            Response.End();
        }
    }
}