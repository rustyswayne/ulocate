﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImportAddressData.ascx.cs" Inherits="uLocate.Web.UserControls.ImportAddressData" %>

<div class="dashboardWrapper">
    <h2>Import Addresses</h2>
    <img src="Dashboard/Images/logo32x32.png" alt="logo" class="dashboardIcon" />

    <asp:PlaceHolder runat="server" ID="phImport">
        <p>
            <span>Select a file to import, then press the <strong>"Import addresses"</strong> button.</span>
        </p>
        <p>
            <asp:FileUpload runat="server" ID="fuImport" />
        </p>
        <p>
            <asp:Button runat="server" ID="btnImport" OnClick="btnImport_Click" Text="Import addresses" />
        </p>
    </asp:PlaceHolder>

    <asp:PlaceHolder runat="server" ID="phFeedback" Visible="false">
        <p>
            <asp:Literal runat="server" ID="ltrlFeedback" />
        </p>
    </asp:PlaceHolder>
</div>