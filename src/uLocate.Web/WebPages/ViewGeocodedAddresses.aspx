﻿<%@ Page Language="C#" AutoEventWireup="true" masterpagefile="~/Umbraco/masterpages/umbracoPage.Master"  CodeBehind="ViewGeocodedAddresses.aspx.cs" Inherits="uLocate.Web.WebPages.ViewGeocodedAddresses" %>
<%@ register namespace="umbraco.uicontrols" assembly="controls" tagprefix="umb" %>
<%@ Import Namespace="umbraco" %>
<%@ Import Namespace="Umbraco.Core" %>
<%@ Import Namespace="uLocate.Core" %>
<%@ Import Namespace="uLocate.Core.Models" %>
<%@ Import Namespace="uLocate.Core.Extensions" %>

<asp:content id="content" contentplaceholderid="body" runat="server">

<umb:umbracopanel id="uPanel" runat="server" hasmenu="true" text="Geocoded Address">

    <umb:pane id="pane" runat="server">


        <asp:repeater id="rpt" runat="server">
            <headertemplate>
            <table>
            <tbody rules="rows" border="0" class="ulocate">
            <thead>
                <tr>
                    <th><%= umbraco.ui.Text("name") %></th>
                    <th>Address</th>
                    <th>Locality</th>
                    <th>Region</th>
                    <th>Postal code</th>
                    <th>Country</th>
                    <th>Geocoded</th>
            	</tr>
            </thead>            
            <tbody>
            </headertemplate>
            <itemtemplate>
                <tr>
                    <td><a href="EditGeocodedAddress.aspx?id=<%# Eval("Id") %>"><%# Eval("Name") %></a></td>
                    <td><%# (Eval("Address1") + " " + Eval("Address2")).Trim() %></td>
                    <td><%# Eval("Locality") %></td>
                    <td><%# Eval("Region") %></td>
                    <td><%# Eval("PostalCode") %></td>
                    <td><%# Eval("CountryCode") %></td>    
                        <td style="text-align:center;">
                            <img src="<%# ((GeocodedAddress)GetDataItem()).Coordinate.IsZeroZero() ? GlobalSettings.Path + "/images/false.png" : GlobalSettings.Path + "/images/true.png" %>" />
                        </td>
                </tr>
            </itemtemplate>
            <footertemplate>
                    </tbody>
                </table>
            </footertemplate>
                    
        </asp:repeater>

    </umb:pane>

</umb:umbracopanel>
</asp:content>