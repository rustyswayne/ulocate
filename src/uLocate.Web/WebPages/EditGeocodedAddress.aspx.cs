﻿using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using uLocate.Core;
using uLocate.Core.Models;
using uLocate.Core.Services;
using umbraco;
using Umbraco.Web.UI.Pages;
using Umbraco.Web.UI;
using umbraco.cms.businesslogic.datatype;
using umbraco.editorControls;

namespace uLocate.Web.WebPages
{
    public partial class EditGeocodedAddress : UmbracoEnsuredPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var id = int.Parse(Request["id"]);

                // populate the country box
                ddlCountryCode.DataSource = ULocateHelper.CountryCodes();
                ddlCountryCode.DataTextField = "key";
                ddlCountryCode.DataValueField = "value";
                ddlCountryCode.DataBind();
                ddlCountryCode.Items.Insert(0, new ListItem(string.Concat(ui.Text("choose"), "..."), string.Empty));

                if (!string.IsNullOrEmpty(Constants.Configuration.DefaultCountryCode))
                    ddlCountryCode.SelectedValue = Constants.Configuration.DefaultCountryCode;

                var address = ULocateHelper.GeocodedAddress(id);

                if (address != null)
                {
                    txtName.Text = address.Name;
                    txtAddress1.Text = address.Address1;
                    txtAddress2.Text = address.Address2;
                    txtLocality.Text = address.Locality;
                    txtRegion.Text = address.Region;
                    txtPostalCode.Text = address.PostalCode;

                    ddlCountryCode.SelectedValue = address.CountryCode;

                    txtTelephone.Text = address.Telephone;
                    txtFax.Text = address.Fax;
                    txtEmail.Text = address.Email;
                    txtWebsiteUrl.Text = address.WebsiteUrl;
                    txtComment.Text = address.Comment;

                    litLat.Text = address.Coordinate.Latitude.ToString();
                    litLong.Text = address.Coordinate.Longitude.ToString();
                }
            }
            else
            {
                // save the values;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.RegisterEmbeddedClientResource(typeof(EditGeocodedAddress), "uLocate.Web.Shared.Scripts.bgGoogle.js", ClientDependencyType.Javascript);
            this.RegisterEmbeddedClientResource(typeof(EditGeocodedAddress), "uLocate.Web.Shared.Styles.uLocate.css", ClientDependencyType.Css);

            var save = uPanel.Menu.NewImageButton();
            save.ImageUrl = string.Concat(GlobalSettings.Path, "/images/editor/save.gif");
            save.AlternateText = "Save (without updating geocode)";
            save.ToolTip = save.AlternateText;

            save.Click += new ImageClickEventHandler(Save_Click);

            var saveGeocode = uPanel.Menu.NewImageButton();
            saveGeocode.ImageUrl = string.Concat(GlobalSettings.Path, "/images/editor/SaveAndPublish.png");
            saveGeocode.AlternateText = "Save & Update Geocode";
            saveGeocode.ToolTip = saveGeocode.AlternateText;

            saveGeocode.Click += new ImageClickEventHandler(SaveGeocode_Click);
        }

        protected void Save_Click(object sender, ImageClickEventArgs e)
        {
            Save(updateGeocode: false);
        }

        protected void SaveGeocode_Click(object sender, ImageClickEventArgs e)
        {
            Save(updateGeocode: true);
        }

        private void Save(bool updateGeocode = true)
        {
            var address = ULocateHelper.GeocodedAddress(int.Parse(Request["id"]));
            address.Name = txtName.Text;
            address.Address1 = txtAddress1.Text;
            address.Address2 = txtAddress2.Text;
            address.Locality = txtLocality.Text;
            address.Region = txtRegion.Text;
            address.PostalCode = txtPostalCode.Text;
            address.Comment = txtComment.Text;
            address.CountryCode = ddlCountryCode.SelectedValue;

            address.WebsiteUrl = txtWebsiteUrl.Text;
            address.Email = txtEmail.Text;
            address.Telephone = txtTelephone.Text;
            address.Fax = txtFax.Text;

            if (updateGeocode)
            {
                address.GeocodeStatus = GeocodeStatus.NOT_QUERIED;
            }

            var service = new GeocodedAddressService();
            service.Save(address);            

            litLat.Text = address.Coordinate.Latitude.ToString(CultureInfo.InvariantCulture);
            litLong.Text = address.Coordinate.Longitude.ToString(CultureInfo.InvariantCulture);

            ClientTools.ShowSpeechBubble(SpeechBubbleIcon.Success, "Save Successful", string.Concat(address.Name, " was successfully saved."));
        }
    }
}