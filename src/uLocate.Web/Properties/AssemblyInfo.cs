﻿using System.Reflection;
using System.Web.UI;
using uLocate.Core;

[assembly: AssemblyTitle("uLocate.Web")]
[assembly: AssemblyDescription("")]


[assembly: WebResource("uLocate.Web.Shared.Styles.uLocate.css", Constants.MediaTypeNames.Text.Css, PerformSubstitution = true)]
[assembly: WebResource("uLocate.Web.Shared.Scripts.bgGoogle.js", Constants.MediaTypeNames.Application.JavaScript)]
